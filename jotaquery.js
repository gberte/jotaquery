function JotaObject(object){        
    this.o = object;
}

JotaObject.prototype.on = function(evname, callback) {
    if (this.o instanceof HTMLCollection){
        for(let i=0; i< this.o.length; i++){
            this.o[i].addEventListener(evname,function(e){
                callback(e);
            });
        }
    }else{
        this.o.addEventListener(evname,function(e){
            callback(e);
        });
    }
}

var $ = function(id){
    let first = id.substring(0, 1);
    let rest = id.substring(1);
    let obj = null;
    if(first=="#"){
        obj =new JotaObject(document.getElementById(rest));
    }else if(first=="."){
        obj =new JotaObject(document.getElementsByClassName(rest));
    }        
    return obj;
}

/*EXAMPLE
$("#id").on("click",function(e){
    e.preventDefault();
    //Do thomething
});

$(".class").on("click",function(e){
    e.preventDefault();
    //Do thomething
});
*/